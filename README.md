# Docker Container Name

Docker container that enables

- JDK8
- JFX8
- Maven

for your CI/CD jobs

## Getting Started

These instructions will cover usage information and for the docker container 

### Prerequisities


In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

This container is only made to be use as CI/CD base image

#### Environment Variables

* `VARIABLE_ONE` - A Description
* `ANOTHER_VAR` - More Description
* `YOU_GET_THE_IDEA` - And another

#### Volumes

* `/your/file/location` - File location

#### Useful File Locations

* `/some/special/script.sh` - List special scripts
  
* `/magic/dir` - And also directories

## Built With

* Oracle JDK 8
* Java FX 8
* Maven v?

## Find Us

* [GitLab](https://gitlab.com/adrianpothuaud-sf/)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://github.com/your/repository/tags). 

## Authors

* **Adrian Pothuaud** - *Initial work*

See also the list of [contributors](https://github.com/your/repository/contributors) who 
participated in this project.

## License

This project is licensed under the ??? License

## Acknowledgments

* People you want to thank

* If you took a bunch of code from somewhere list it here

- http://www.mymiller.name/wordpress/docker/javafx-maven-docker-image-openjfx/